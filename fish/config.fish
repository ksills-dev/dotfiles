set -x PATH $PATH ~/Programming/tools
set -x PATH $PATH ~/.cargo/bin

set -g n_clears -1         # How many times an empty command has been entered.
set -g last_cmd ""         # The last command dispatched by fish.
set -g last_cmd_was_fish 0 # Whether or not the last command was fish.
                           # (used to determine if empty command given).

# `_` is a variable containing the current executable. Changes to the program
# being run when a command is issued - changes back to fish at the end.
function fish_exec_change --on-variable _
  if not test "fish" = $_            # A 'real' command has been entered.
    set -g last_cmd $_
    set -g last_cmd_was_fish 0
  else
    if test $last_cmd_was_fish -eq 1 # If two `_` transitions in a row are fish
                                     # the user has either entered an empty
                                     # command or run fish (unlikely).
      set -g last_cmd
    end
    set -g last_cmd_was_fish 1
  end
end

function fish_prompt
  # Postexec prompt (visual break, command, TTR (ms), and time).
  set -l lstat $status # Store the (un)successfulness of execution.
  if test -z $last_cmd
    set -g n_clears (math "$n_clears+1")
    if test $n_clears -eq 3
      set -g n_clears 0
      command clear
    end
  else
    set -g n_clears 0

    if test $lstat -eq 0
      set_color -d blue
    else if test $lstat -eq 130
      set_color -d yellow
    else
      set_color -d red
    end

    set -l dur "$CMD_DURATION"
    set -l ctime (date --date='now' "+%T")
    set -l msg "$last_cmd-[$dur ms]-$ctime"
    set -l msg_size (string length $msg)
    for i in (seq (math "$COLUMNS-$msg_size"))
      echo -n "─"
    end
    echo "$msg"
  end

  # Actual Prompt

  set_color normal
  echo -n "╭╼ In "
  set_color green
  echo -n (string match -a -r '/[^/]+$' (pwd))
  set_color normal
  echo -n " as "
  set_color yellow
  echo -n $USER
  set_color normal
  echo -n " on "
  set_color yellow
  echo -n (hostname)

  # Provides git repository information if it's available.

  set -l git_info (eval __fish_git_prompt)
  if test $git_info
    set_color normal
    echo -n " ***"
    set_color yellow
    echo -n $git_info
  end

  # Leading indicator for command.

  echo ""
  set_color normal
  echo -n "└╼┫ "
end
