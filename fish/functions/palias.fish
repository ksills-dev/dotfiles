# Persistant Alias
function palias
  # Check for help request
  # Check number of args

  set -l tmp "function $argv[1]\n  eval \"$argv[2]"
  if test (math ""(count $argv)" > 2") -eq 1
    set -l tmp $tmp" argv[3..-1]"
  end
  set -l tmp $tmp" \$argv"
  set -l tmp $tmp"\"\nend\n"

  set -l filename "$HOME/.config/fish/functions/$argv[1].fish"

  # Check if file exists
  if test -e $filename
    if ask "File exists. Overwrite?"
      rm $filename
    else
      echo "Cancelling..."
      return
    end
  end
  
  touch $filename
  echo -e $tmp > $filename
  and echo "Successively created alias."
  or  echo "Failed to create alias."
end
