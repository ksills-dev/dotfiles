# `ask`
# -----
# Usage: `ask message`
function ask
  # Check number of arguments
  set -l valid_answer 0
  set -l response 1

  while test $valid_answer -eq 0
    read -P"> $argv[1] [y/n]: "  response

    switch $response[1]
      case 'y' 'Y'
        set valid_answer 1
        set response 0
      case 'n' 'N'
        set valid_answer 1
        set response 1
      case '*'
        echo "Please provide a y/n answer..."
        set valid_answer 0
    end
  end

  return $response
end

