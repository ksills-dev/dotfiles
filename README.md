# Dotfile Configurations
Files in this repository are miscellaneous configuration and settings files used
between my machines.

## Vim
My vim configuration (relies on VimPlug).

## i3
Configuration for the i3 window manager. Two variations are provided:
* **desktop** for my Tiny Tim.
* **laptop** for both of my laptop setups.

## Kitty
The configuration used for the kitty terminal.

## Fish 
Includes the fish shell startup and all the registered function files.
