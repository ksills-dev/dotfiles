" Indentation rules
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent

" Indentation Guides
set cursorline
set cc=81
highlight ColorColumn ctermbg=DarkGray
let g:indent_guides_auto_colors=0

" Some Generic Stuffs
set relativenumber
set number
set nowrap

" Keybindings
let mapleader=','
map <leader>f :NERDTreeToggle<CR>
map <leader>p "+p
map <leader>s :w<CR>

" Clang-Format options
let g:clang_format#code_style='llvm'

" Syntastic Custom Settings
map <leader>c :SyntasticCheck<CR>
let g:syntastic_cpp_checkers = ['clang_check']
let g:syntastic_cpp_clang_check_args = "-extra-arg=--std=c++17"

" Airline theme
let g:airline_solarized_bg='dark'
let g:airline_theme='solarized'

" Plugins
call plug#begin('~/.local/share/nvim/plugged')

" Tagbar
map <leader>tb :TagbarToggle<CR>

Plug 'terryma/vim-multiple-cursors'

Plug 'stfl/meson.vim'
Plug 'dag/vim-fish'
Plug 'zah/nim.vim'
Plug 'phpactor/phpactor'

Plug 'rhysd/vim-clang-format'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-surround'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'scrooloose/syntastic'

Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'yggdroot/indentline'
Plug 'majutsushi/tagbar'
Plug 'justinmk/vim-syntax-extra'

Plug 'loskutov/vim-input-latex'

Plug 'Shougo/deoplete.nvim'
Plug 'kristijanhusak/deoplete-phpactor'
"Plug 'valloric/youcompleteme'

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PHP Actor Bindings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Invoke the context menu
nmap <Leader>mm :call phpactor#ContextMenu()<CR>

" Invoke the navigation menu
nmap <Leader>nn :call phpactor#Navigate()<CR>

" Goto definition of class or class member under the cursor
nmap <Leader>oo :call phpactor#GotoDefinition()<CR>
